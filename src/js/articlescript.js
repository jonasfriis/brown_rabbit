window.addEventListener("DOMContentLoaded", start);

async function start() {
  let orderData = await getJson();
  let urlIndex = getUrlIndex();
  setArticle(orderData, urlIndex);
}

async function getJson() {
  const url = "../content.json";
  const response = await fetch(url);
  const jsonData = await response.json();

  console.log(jsonData);
  return jsonData;
}

function getUrlIndex() {
  const url = window.location.href;
  const index = url.substring(url.indexOf("?") + 1, url.length);
  console.log(index);
  return index;
}

function setArticle(orderData, urlIndex) {
  const container = document.querySelector("#article");

  container.querySelector("img").src = orderData.articles[urlIndex].img;
  container.querySelector("h2").textContent = "Article " + (urlIndex + 1);
  container.querySelector("p").textContent = orderData.articles[urlIndex].text;
}
